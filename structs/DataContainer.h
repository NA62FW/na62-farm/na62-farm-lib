/*
 * DataContainer.h
 *
 *  Created on: Oct 23, 2015
 *      Author: Jonas Kunze (kunze.jonas@gmail.com)
 */

#pragma once

#include <netinet/in.h>
#include <sys/types.h>
#include <algorithm>
#include <cstdint>

#include "../options/Logging.h"

namespace na62 {
struct DataContainer {
	char * data;
	uint_fast16_t length;
	bool ownerMayFreeData;

	uint16_t checksum;

	DataContainer() :
			data(nullptr), length(0), ownerMayFreeData(false), checksum(0) {
	}

	DataContainer(char* _data, uint_fast16_t _length, bool _ownerMayFreeData);

	~DataContainer() {
	}

	/**
	 * Copy constructor
	 */
	DataContainer(const DataContainer& other) :
			data(other.data), length(std::move(other.length)), ownerMayFreeData(
					other.ownerMayFreeData), checksum(other.checksum) {
	}

	/**
	 * Copy constructor
	 */
	DataContainer(const DataContainer&& other) :
			data(other.data), length(other.length), ownerMayFreeData(
					other.ownerMayFreeData), checksum(other.checksum) {
	}

	/**
	 * Move assignment operator
	 */
	DataContainer& operator=(DataContainer&& other) {
		if (&other != this) {
			data = other.data;
			length = other.length;
			ownerMayFreeData = other.ownerMayFreeData;
			checksum = other.checksum;

			other.data = nullptr;
			other.length = 0;
		}
		return *this;
	}

	/**
	 * Move assignment operator
	 */
	DataContainer& operator=(DataContainer& other) {
		if (&other != this) {
			data = other.data;
			length = other.length;
			ownerMayFreeData = other.ownerMayFreeData;
			checksum = other.checksum;
		}
		return *this;
	}

	bool checkValid();


	void inline free() {
		//checkValid();
		if (ownerMayFreeData) {
			checksum = 0;
			if (data == nullptr) {
				LOG_WARNING("data was already freed");
			} else {
				delete[] data;
			}
			data = nullptr;
		}
	}
};

}
