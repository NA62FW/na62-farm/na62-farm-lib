
#include <new>
#include "Subevent.h"

extern "C" {
    void* create_subevent() {
        uint const max_fragment_number = 32;
        na62::l0::Subevent* subevent = new na62::l0::Subevent(max_fragment_number);
        return reinterpret_cast<void*>(subevent);
    }


    void set_subevent(void* pointer, char* start_data, char* end_data) {
        na62::l0::Subevent* subevent = reinterpret_cast<na62::l0::Subevent*>(pointer);
        subevent->reset();
        char* rolling = start_data;
        while(rolling < end_data) {  // LOOP ON ALL SUBDETECTOR BLOCKS
          na62::l0::MEPFragment_HDR* fragment_header = reinterpret_cast<na62::l0::MEPFragment_HDR*>(rolling);
          if (not subevent->addFragment(fragment_header)) {
              std::cout << "Maximum number of fragments exceeded" << std::endl;
          }
          rolling += fragment_header->eventLength_;
        }
    }

    void delete_subevent(void* pointer) {
        delete reinterpret_cast<na62::l0::Subevent*>(pointer);
    }
}


namespace na62 {
namespace l0 {


Subevent::Subevent(const uint_fast16_t pexpectedPacketsNum):
    expectedPacketsNum(pexpectedPacketsNum), sourceID(0), fragmentCounter(0), eventFragments(new (std::nothrow) MEPFragment*[pexpectedPacketsNum]) {

    for (uint index = 0; index < expectedPacketsNum; ++index) {
        eventFragments[index] = new MEPFragment();
    }
}

Subevent::~Subevent() {
    for (uint index = 0; index < expectedPacketsNum; ++index) {
        delete eventFragments[index];
    }
    delete[] eventFragments;
}

} /* namespace l0 */
} /* namespace na62 */
