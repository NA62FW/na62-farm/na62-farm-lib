/*
 * BurstFileWriter.cpp
 *
 *  Created on: Mar 20, 2015
 *      Author: Jonas Kunze (kunze.jonas@gmail.com)
 */

#include "BurstFileWriter.h"

#include <sstream>

#include <boost/date_time/microsec_time_clock.hpp>
#include <boost/date_time/posix_time/posix_time_config.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/date_time/time.hpp>
#include <boost/date_time/time_duration.hpp>
#include <boost/filesystem.hpp>

#include <pwd.h>
#include <grp.h>
#include <unistd.h>
#include <cstdlib>
#include <ctime>


#include "../options/Logging.h"
#include "../structs/BurstFile.h"
#include "../utils/Utils.h"
#include "structs/Event.h"


namespace na62 {

BurstFileWriter::BurstFileWriter(const std::string filePath,
		const uint numberOfEvents, const uint sob,
		const uint runNumber, const uint burstID) :
		myFile_(filePath.data(),
				std::ios::out | std::ios::trunc | std::ios::binary), filePath_(
				filePath), eventID_(0) {

	if (!myFile_.good()) {
		LOG_ERROR("Unable to write to file " << filePath);
		exit(1);
	}

	/*
	 * Generate the burst file header
	 */
	const uint headerLength = BURST_HDR::calculateHeaderSize(numberOfEvents);
	//https://stackoverflow.com/questions/7546620/operator-new-initializes-memory-to-zero
	hdr_ = reinterpret_cast<BURST_HDR *>(new char[headerLength]());  // zero    initialized (ie all elements set to 0)

	hdr_->fileFormatVersion = 2;
	hdr_->zero = 0;

	hdr_->numberOfEvents = numberOfEvents;
	hdr_->runID = runNumber;
	hdr_->burstID = burstID;

	eventNumbers_ = hdr_->getEventNumbers();
	triggerWords_ = hdr_->getEventTriggerTypeWords();
	offsets_ = hdr_->getEventOffsets();

	bytesWritten_ = headerLength;

	stopWatch_.start();
#ifdef WRITE_HDR
	// jump to the first byte behind the header
	myFile_.seekp(headerLength);
#endif
}

BurstFileWriter::~BurstFileWriter() {

}

void
BurstFileWriter::flushFile(uint32_t counter) {
	const uint allocated_size = hdr_->getHeaderSize(); // TODO class member
	
	// defragmenting the header
	memcpy(reinterpret_cast<void*>(hdr_->getEventNumbers() + counter), reinterpret_cast<void*>(hdr_->getEventTriggerTypeWords()), counter * 4);
	std::memset(reinterpret_cast<void*>(hdr_->getEventTriggerTypeWords()), 0, counter * 4);
	memcpy(reinterpret_cast<void*>(hdr_->getEventNumbers() + counter + counter), reinterpret_cast<void*>(hdr_->getEventOffsets()), counter * 4);
	std::memset(reinterpret_cast<void*>(hdr_->getEventOffsets()), 0, counter * 4);

	hdr_->numberOfEvents = counter;
	
#ifdef WRITE_HDR
	// Write the header to the beginning
	myFile_.seekp(0);
	myFile_.write(reinterpret_cast<const char*>(hdr_), allocated_size);
	myFile_.close();
#endif
	boost::posix_time::ptime stop(
			boost::posix_time::microsec_clock::local_time());

	long msec = stopWatch_.elapsed().wall / 1E6;
	long dataRate = 0;
	if (msec != 0) {
		dataRate = bytesWritten_ / msec * 1000; // B/s
	}

	LOG_INFO("Wrote burst " << hdr_->burstID << " with " << hdr_->numberOfEvents << " events and " << bytesWritten_ << "B with " << Utils::FormatSize(dataRate) << "B/s");

	delete[] hdr_;
}

bool BurstFileWriter::doChown(std::string file_path, std::string user_name, std::string group_name) {

	struct passwd* pwd = getpwnam(user_name.c_str());
	if (pwd == NULL) {
		LOG_ERROR("Failed to get uid");
		throw std::runtime_error( "Cannot fetch the uid" );
	}
	uid_t uid = pwd->pw_uid;

	struct group* grp = getgrnam(group_name.c_str());
	if (grp == NULL) {
		LOG_ERROR("Failed to get gid");
		throw std::runtime_error( "Cannot fetch the gid");
	}
	gid_t  gid = grp->gr_gid;

	if (chown(file_path.c_str(), uid, gid) == -1) {
		LOG_ERROR("Chown fail");
		return false;
	}
	return true;
}

void BurstFileWriter::writeEvent(const EVENT_HDR* event) {
	myFile_.write(reinterpret_cast<const char*>(event), event->length * 4);

	eventNumbers_[eventID_] = event->eventNum;
	triggerWords_[eventID_] = event->triggerWord;
	offsets_[eventID_] = bytesWritten_ / 4;
	bytesWritten_ += event->length * 4;
	eventID_++;
}

void  BurstFileWriter::setNumberOfEvents(uint32_t counter) {
	hdr_->numberOfEvents = counter; // set the correct number of events
}

std::string BurstFileWriter::generateOneFileName(uint32_t sob, uint32_t runNumber, uint32_t burstID, uint16_t merger_id, uint16_t duplicate) {
	std::stringstream fileName;

	fileName << "na62raw_" << sob << "-" << std::setfill('0') << std::setw(2) << merger_id;
	fileName << "-" << std::setfill('0') << std::setw(6) << runNumber << "-";
	fileName << std::setfill('0') << std::setw(4) << burstID;

	if (duplicate != 0) {
		fileName << "_" << duplicate;
	}
	fileName << ".dat";
	return fileName.str();
}

// Recurision can be also a strategy...
std::string BurstFileWriter::generateFilePath(std::string dir, uint32_t sob_time, uint32_t run_number, uint32_t burstID, uint16_t merger_id) {
	uint16_t duplicate_count = 0; 
	std::string file_name = generateOneFileName(sob_time, run_number, burstID, merger_id, duplicate_count);
	std::string file_path = dir + file_name;
	// looping until the file does not exist
	while (boost::filesystem::exists(file_path)) {
		LOG_ERROR("File already exists: " << file_path);
		file_name = generateOneFileName(sob_time, run_number, burstID, merger_id, ++duplicate_count);
		file_path = dir + file_name;
	}
	return file_path;
}

}
/* namespace na62 */
