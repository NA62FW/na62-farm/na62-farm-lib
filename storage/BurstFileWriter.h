/*
 * BurstFileWriter.h
 *
 *  Created on: Mar 20, 2015
 *      Author: Jonas Kunze (kunze.jonas@gmail.com)
 */

#pragma once

#include <stddef.h>
#include <sys/types.h>
#include <cstdint>
#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
#include <boost/timer/timer.hpp>


#define WRITE_HDR

namespace na62 {
struct EVENT_HDR;
} /* namespace na62 */

namespace na62 {
struct BURST_HDR;
} /* namespace na62 */

namespace na62 {

class BurstFileWriter {

public:
	BurstFileWriter(const std::string filePath,
			const uint numberOfEvents, const uint sob, const uint runNumber,
			const uint burstID);

	~BurstFileWriter();

	void writeEvent(const EVENT_HDR * event);
	void flushFile(uint32_t counter);
	void setNumberOfEvents(uint32_t counter);
	std::string generateFileName(uint32_t sob, uint32_t runNumber, uint32_t burstID, uint32_t duplicate);


	static bool doChown(std::string file_path, std::string user_name, std::string group_name);
	static std::string generateOneFileName(uint32_t sob, uint32_t runNumber, uint32_t burstID, uint16_t merger_id, uint16_t duplicate);
	static std::string generateFilePath(std::string dir, uint32_t sob, uint32_t runNumber, uint32_t burstID, uint16_t merger_id);

private:
	std::ofstream myFile_;
	std::string filePath_;
	BURST_HDR* hdr_;
	uint32_t* eventNumbers_;
	uint32_t* triggerWords_;
	uint32_t* offsets_;
	size_t bytesWritten_;
	uint eventID_;

	boost::timer::cpu_timer stopWatch_;
};

} /* namespace na62 */

